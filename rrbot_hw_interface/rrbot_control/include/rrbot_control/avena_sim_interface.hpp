#ifndef RRBOT_CONTROL__AVENA_SIM_INTERFACE_HPP
#define RRBOT_CONTROL__AVENA_SIM_INTERFACE_HPP

#include <ros/ros.h>
#include "custom_msgs/GetArmState.h"
#include "custom_msgs/SetArmTorques.h"
#include <optional>
#include <chrono>
namespace rrbot_control
{
/// \brief Hardware interface for a robot
class SimInterface
{
public:
  /**
   * \brief Constructor
   * \param nh - Node handle for topics.
   */
  SimInterface(ros::NodeHandle& nh)
  {
    arm_state_client_ = nh.serviceClient<custom_msgs::GetArmState>("/arm_controller/get_current_arm_state");
    arm_torque_client_ = nh.serviceClient<custom_msgs::SetArmTorques>("/arm_controller/set_torques");
  }

  /** \brief Read the state from the robot simulation. */
  std::optional<std::vector<double>> read()
  {
    auto start = std::chrono::system_clock::now();
    ROS_INFO("reading arm state");
    // read torque
    custom_msgs::GetArmState srv;
    if (!arm_state_client_.call(srv))
    {
      ROS_ERROR("Could not read arm state");
      return std::nullopt;
    }
    std::cout << "READ::  Elapsed time in milliseconds: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count()
         << " ms\n";
    for (size_t jnt_idx = 0; jnt_idx < srv.response.arm_current_positions.size(); jnt_idx++)
      ROS_INFO_STREAM("arm state Joint " << jnt_idx << " : " << srv.response.arm_current_positions.at(jnt_idx));
    return srv.response.arm_current_positions;
  }

  /** \brief Write the command to the robot simulation. */
  void write(const std::vector<double>& joint_effort_command_)
  {
    ROS_INFO("setting toques");
    // write torque
    custom_msgs::SetArmTorques srv;
    for (size_t jnt_idx = 0; jnt_idx < joint_effort_command_.size(); jnt_idx++)
      ROS_INFO_STREAM("arm command Joint " << jnt_idx << " : " << joint_effort_command_.at(jnt_idx));

    if (joint_effort_command_.size() != 0)
    {
      srv.request.torques = joint_effort_command_;
      srv.request.turn_motor = std::vector<int64_t>(joint_effort_command_.size(), 1);
    }
    if (!arm_torque_client_.call(srv))
    {
      ROS_ERROR("Could not localize part");
      return;
    }
    for (size_t jnt_idx = 0; jnt_idx < srv.response.error.size(); jnt_idx++)
      ROS_INFO_STREAM("setting torque is fiinshed: " << srv.response.error.at(jnt_idx));
  }

private:
  ros::ServiceClient arm_state_client_;
  ros::ServiceClient arm_torque_client_;

};  // class
}  // namespace rrbot_control

#endif