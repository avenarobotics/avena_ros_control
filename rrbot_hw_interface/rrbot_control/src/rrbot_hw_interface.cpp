#include <rrbot_control/rrbot_hw_interface.h>

namespace rrbot_control
{
RRBotHWInterface::RRBotHWInterface(ros::NodeHandle& nh, urdf::Model* urdf_model)
  : ros_control_boilerplate::GenericHWInterface(nh, urdf_model), avena_sim_(nh)
{
  ROS_INFO_NAMED("rrbot_hw_interface", "RRBotHWInterface Ready.");
}

void RRBotHWInterface::read(ros::Duration& elapsed_time)
{
  if (auto joint_positions = avena_sim_.read())
    joint_position_ = joint_positions.value();
}

void RRBotHWInterface::write(ros::Duration& elapsed_time)
{
  // Safety
  enforceLimits(elapsed_time);
  // for (std::size_t joint_id = 1; joint_id < num_joints_; joint_id++)
  //   joint_effort_command_[joint_id] = 0.0;
  avena_sim_.write(joint_effort_command_);
  // avena_sim_.write(joint_position_command_);

  // FOR A EASY SIMULATION EXAMPLE, OR FOR CODE TO CALCULATE
  // VELOCITY FROM POSITION WITH SMOOTHING, SEE
  // sim_hw_interface.cpp IN THIS PACKAGE
  //
  // DUMMY PASS-THROUGH CODE
  // for (std::size_t joint_id = 0; joint_id < num_joints_; ++joint_id)
  //   joint_position_[joint_id] += joint_position_command_[joint_id];

  // for (std::size_t joint_id = 0; joint_id < num_joints_; ++joint_id)
  //   joint_effort_[joint_id] += joint_effort_command_[joint_id];
  // END DUMMY CODE
  //
  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------
}

void RRBotHWInterface::enforceLimits(ros::Duration& period)
{
  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------
  //
  // CHOOSE THE TYPE OF JOINT LIMITS INTERFACE YOU WANT TO USE
  // YOU SHOULD ONLY NEED TO USE ONE SATURATION INTERFACE,
  // DEPENDING ON YOUR CONTROL METHOD
  //
  // EXAMPLES:
  //
  // Saturation Limits ---------------------------
  //
  // Enforces position and velocity
  // pos_jnt_sat_interface_.enforceLimits(period);
  //
  // Enforces velocity and acceleration limits
  // vel_jnt_sat_interface_.enforceLimits(period);
  //
  // Enforces position, velocity, and effort
  eff_jnt_sat_interface_.enforceLimits(period);

  // Soft limits ---------------------------------
  //
  // pos_jnt_soft_limits_.enforceLimits(period);
  // vel_jnt_soft_limits_.enforceLimits(period);
  // eff_jnt_soft_limits_.enforceLimits(period);
  //
  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------
}

}  // namespace rrbot_control
