### Avena ROS Control

## Installation

1. http://wiki.ros.org/noetic/Installation/Ubuntu

2. sudo apt-get install ros-noetic-ros-control ros-noetic-ros-controllers

3. sudo apt-get install ros-noetic-rosparam-shortcuts

4. sudo apt-get install libgflags-dev

5. sudo apt install ros-noetic-xacro

6. sudo apt install ros-noetic-robot-state-publisher
## Running

1. roslaunch rrbot_hw_interface rrbot_simulation.launch 

2. roslaunch rrbot_hw_interface rrbot_visualize.launch

3. roslaunch rrbot_hw_interface rrbot_test_trajectory.launch

## Troubleshooting

1. killall -9 rosmaster
